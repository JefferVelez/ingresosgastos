﻿namespace IngresosGastos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IngresosGastosJVPs", "EsIngreso", c => c.Boolean(nullable: false));
            DropColumn("dbo.IngresosGastosJVPs", "Ingreso");
        }
        
        public override void Down()
        {
            AddColumn("dbo.IngresosGastosJVPs", "Ingreso", c => c.Boolean(nullable: false));
            DropColumn("dbo.IngresosGastosJVPs", "EsIngreso");
        }
    }
}
