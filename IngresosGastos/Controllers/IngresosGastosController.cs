﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IngresosGastos.Data;
using IngresosGastos.Models;

namespace IngresosGastos.Controllers
{
    public class IngresosGastosController : Controller
    {
        private AppDBContext db = new AppDBContext();

        // GET: IngresosGastos
        public ActionResult Index()
        {
            double ingresos, gastos, neto;

            ingresos = db.IngresosGastos.Where(m => m.EsIngreso).Select(p => p.Valor).Sum();
            gastos = db.IngresosGastos.Where(m => !m.EsIngreso).Select(p => p.Valor).Sum();

            neto = ingresos - gastos;

            ViewBag.Ingresos = ingresos;
            ViewBag.Gastos = gastos;
            ViewBag.Neto = neto;



            return View(db.IngresosGastos.ToList());
        }

        // GET: IngresosGastos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IngresosGastosJVP ingresosGastosJVP = db.IngresosGastos.Find(id);
            if (ingresosGastosJVP == null)
            {
                return HttpNotFound();
            }
            return View(ingresosGastosJVP);
        }

        // GET: IngresosGastos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IngresosGastos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Descripcion,EsIngreso,Valor")] IngresosGastosJVP ingresosGastosJVP)
        {
            if (ModelState.IsValid)
            {
                db.IngresosGastos.Add(ingresosGastosJVP);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ingresosGastosJVP);
        }

        // GET: IngresosGastos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IngresosGastosJVP ingresosGastosJVP = db.IngresosGastos.Find(id);
            if (ingresosGastosJVP == null)
            {
                return HttpNotFound();
            }
            return View(ingresosGastosJVP);
        }

        // POST: IngresosGastos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Descripcion,EsIngreso,Valor")] IngresosGastosJVP ingresosGastosJVP)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ingresosGastosJVP).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ingresosGastosJVP);
        }

        // GET: IngresosGastos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IngresosGastosJVP ingresosGastosJVP = db.IngresosGastos.Find(id);
            if (ingresosGastosJVP == null)
            {
                return HttpNotFound();
            }
            return View(ingresosGastosJVP);
        }

        // POST: IngresosGastos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IngresosGastosJVP ingresosGastosJVP = db.IngresosGastos.Find(id);
            db.IngresosGastos.Remove(ingresosGastosJVP);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
